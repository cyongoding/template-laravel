@include('adminlte.head')

    <!-- SEARCH FORM -->
   @include('adminlte.form')


  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 @include('adminlte.main-sidebar')

      <!-- Sidebar Menu -->
@include('adminlte.sidebar')
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   @include('adminlte.contenthead')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
@include('adminlte.box')
 @include('adminlte.tugas1')      
        <!-- /.card-body -->

        <!-- /.card-footer-->
      
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('adminlte.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>


</body>
</html>
